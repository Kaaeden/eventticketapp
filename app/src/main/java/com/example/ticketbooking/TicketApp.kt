package com.example.ticketbooking
import android.app.Application;
import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
class TicketApp: Application()
