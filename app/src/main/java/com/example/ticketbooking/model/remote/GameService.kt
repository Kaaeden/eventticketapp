package com.example.ticketbooking.model.remote

import com.example.ticketbooking.model.local.dtos.MatchDetailsDTO
import com.example.ticketbooking.model.local.entities.MatchDetails
import retrofit2.http.GET

interface GameService {

    @GET("cdn-og-test-api/test-task/fixtures.json")
    suspend fun getMatchTicketData(): List<MatchDetailsDTO>

}