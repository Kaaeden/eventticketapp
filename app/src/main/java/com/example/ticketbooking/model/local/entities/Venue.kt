package com.example.ticketbooking.model.local.entities

data class Venue(
    val id: Int = 0,
    val name: String = ""
)