package com.example.ticketbooking.model.mapper

import com.example.ticketbooking.model.local.dtos.MatchDetailsDTO
import com.example.ticketbooking.model.local.entities.MatchDetails

class MatchDetailMapper: Mapper<MatchDetailsDTO, MatchDetails> {
    override fun invoke(dto: MatchDetailsDTO): MatchDetails = with(dto) {
        return MatchDetails(
            id = id,
            type = type,
            homeTeam = homeTeam,
            awayTeam = awayTeam,
            date = date,
            competitionStage = competitionStage,
            venue = venue,
            state = state
            )
    }
}