package com.example.ticketbooking.model.local.entities

data class HomeTeam(
    val abbr: String = "",
    val alias: String = "",
    val id: Int = 0,
    val name: String = "",
    val shortName: String = ""
)