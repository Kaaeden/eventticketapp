package com.example.ticketbooking.model.local.entities

data class AwayTeam(
    val abbr: String = "",
    val alias: String = "",
    val id: Int = 0,
    val name: String = "",
    val shortName: String = ""
)