package com.example.ticketbooking.model.remote

import com.example.ticketbooking.model.local.dtos.MatchDetailsDTO
import com.example.ticketbooking.model.local.entities.MatchDetails
import com.example.ticketbooking.model.mapper.MatchDetailMapper
import javax.inject.Inject

class MatchRepo @Inject constructor(private val service: GameService){

    private val matchMapper: MatchDetailMapper = MatchDetailMapper()

    suspend fun getMatchDataFromRepo(): List<MatchDetails> {

        val repoResponse = service.getMatchTicketData()
        return repoResponse.map{matchMapper(it)}
    }

}