package com.example.ticketbooking.model.local.dtos

import com.example.ticketbooking.model.local.entities.AwayTeam
import com.example.ticketbooking.model.local.entities.CompetitionStage
import com.example.ticketbooking.model.local.entities.HomeTeam
import com.example.ticketbooking.model.local.entities.Venue

data class MatchDetailsDTO(
    val id: String = "",
    val type: String = "",
    val homeTeam: HomeTeam = HomeTeam(),
    val awayTeam: AwayTeam = AwayTeam(),
    val date: String = "",
    val competitionStage: CompetitionStage = CompetitionStage(),
    val venue: Venue = Venue(),
    val state: String = "") {
}