package com.example.ticketbooking.model.local.entities

class MatchDetails(
    val id: String = "",
    val type: String = "",
    val homeTeam: HomeTeam = HomeTeam(),
    val awayTeam: AwayTeam = AwayTeam(),
    val date: String = "",
    val competitionStage: CompetitionStage = CompetitionStage(),
    val venue: Venue = Venue(),
    val state: String = ""

) {
}