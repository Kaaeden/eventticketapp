package com.example.ticketbooking.model.local.entities

data class Competition(
    val id: Int = 0,
    val name: String = ""
)