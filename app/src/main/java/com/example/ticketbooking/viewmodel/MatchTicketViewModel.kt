package com.example.ticketbooking.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ticketbooking.model.remote.MatchRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@HiltViewModel
class MatchTicketViewModel @Inject constructor(private val repo: MatchRepo): ViewModel() {

    private val _state = MutableStateFlow(MatchTicketState())
    val state get() = _state.asStateFlow()

    fun getMatchTicketDetails(){
        viewModelScope.launch{
            _state.update{ it.copy(isLoading = true) }
            val matchDetails = repo.getMatchDataFromRepo()
            _state.update{ it.copy(matchTickets = matchDetails, isLoading = false)}

        }
    }
}