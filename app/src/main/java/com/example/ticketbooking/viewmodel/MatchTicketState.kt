package com.example.ticketbooking.viewmodel

import com.example.ticketbooking.model.local.entities.MatchDetails

data class MatchTicketState(
    val isLoading: Boolean = false,
    val matchTickets: List<MatchDetails> = emptyList(),
    val onError: String = ""

){}
