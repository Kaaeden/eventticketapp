package com.example.ticketbooking

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ticketbooking.model.local.entities.MatchDetails
import com.example.ticketbooking.ui.theme.TicketBookingTheme
import com.example.ticketbooking.viewmodel.MatchTicketViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val matchViewModel by viewModels<MatchTicketViewModel>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        matchViewModel.getMatchTicketDetails()
        setContent {
            val state = matchViewModel.state.collectAsState()
            TicketBookingTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    LazyColumn(){items(state.value.matchTickets){ matchTix -> Column(){
                        //Text(matchTix.competitionStage.competition.name)
                        TicketPallette(matchData = matchTix)

                    }
                       } }
                }
            }
        }
    }
}


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TicketPallette(matchData: MatchDetails){

    var ticketDate = matchData.date
    var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    var localDate = LocalDateTime.parse(ticketDate, formatter)
    var formatter2 = DateTimeFormatter.ofPattern("MMM dd, yyyy 'at' HH:mm")
    var formattedDate = localDate.format(formatter2)
    var formatter3 = DateTimeFormatter.ofPattern("EE")
    var dayOfWeek = localDate.format(formatter3)
    var formatter4 = DateTimeFormatter.ofPattern("dd")
    var dayOfMonth = localDate.format(formatter4)
    Card(elevation = CardDefaults.cardElevation(15.dp), colors = CardDefaults.cardColors(Color.White), shape = CutCornerShape(2.dp)) {
        Box() {
            Column() {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp)) {
                    Box(modifier = Modifier.weight(1f)) {
                        Column() {

                            Text(
                                matchData.competitionStage.competition.name,
                                color = Color.Black,
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold
                            )
                            Row(modifier = Modifier){
                                Text(
                                    matchData.venue.name + " | ",
                                    color = Color.Gray,
                                    fontSize = (13.sp)
                                )
                                if (matchData.state == "postponed") {
                                    Text(formattedDate,
                                        color = Color(221, 75, 7, 255),
                                        fontSize = (13.sp))
                                }
                                else {
                                    Text(formattedDate,
                                        color = Color.Gray,
                                        fontSize = (13.sp))
                                }

                            }

                        }
                    }
                    Box(modifier = Modifier.weight(.40f).padding(10.dp), contentAlignment = Alignment.Center) {

                        if (matchData.state == "postponed") {
                            Column() {
                                Box(
                                    modifier = Modifier
                                        .background(Color(221, 75, 7, 255))
                                        .width(80.dp)
                                        .height(20.dp)
                                ) {
                                    Text("POSTPONED", color = Color.White, fontSize = 13.sp)
                                }
                            }
                        }

                    }
                }
                Row(modifier = Modifier.fillMaxWidth()) {
                    Box(contentAlignment = Alignment.CenterStart, modifier = Modifier.weight(1f)) {
                        Column() {

                            Text(matchData.homeTeam.name, fontSize = 20.sp, modifier = Modifier.padding(20.dp), fontWeight = FontWeight.Bold)
                            Text(matchData.awayTeam.name, modifier = Modifier.padding(20.dp), fontSize = 20.sp, fontWeight = FontWeight.Bold)
                        }

                    }
                    Divider(
                        color = Color.Gray,
                        modifier = Modifier
                            .fillMaxHeight()
                            .width(1.dp)
                            .height(100.dp)
                    )
                    Box(modifier = Modifier.weight(.40f).padding(10.dp), contentAlignment = Alignment.Center,) {

                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            Text(
                                dayOfMonth, fontSize = 40.sp, color = Color(
                                    5,
                                    22,
                                    65,
                                    255
                                )
                            )
                            Text(
                                dayOfWeek,
                                fontWeight = FontWeight.Bold,
                                fontSize = 30.sp,
                                color = Color(
                                    5,
                                    22,
                                    65,
                                    255
                                )
                            )

                        }
                    }


                }
            }

        }
    }
}